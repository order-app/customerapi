package service

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	errors2 "orderapplication/src/pkg/helper/errors"
	"orderapplication/src/storage/mongoDb"
	"orderapplication/src/storage/type"
)

type Service struct {
	repo *mongoDb.Repository
}

func NewService(repo *mongoDb.Repository) *Service {
	return &Service{repo: repo}
}

func (s *Service) FindCustomerById(id primitive.ObjectID) *_type.Customer {
	res := s.repo.FindById(id)
	if res == nil {
		errors2.Panic(errors2.CustomerNotFoundError)
	}
	return res
}

func (s *Service) FindCustomers(options *options.FindOptions, filter map[string]interface{}) (int, []_type.Customer) {
	total, res := s.repo.FindMany(filter, options)
	if res == nil {
		errors2.Panic(errors2.CustomerNotFoundError)
	}
	return int(total), res
}

func (s *Service) IsCustomerIdValid(id primitive.ObjectID) int64 {
	res := s.repo.IsIdValid(id)
	if res == 0 {
		errors2.Panic(errors2.CustomerNotFoundError)
	}
	return res
}

func (s *Service) CreateCustomer(customer _type.Customer) interface{} {
	res := s.repo.InsertOne(customer)
	if res == nil {
		errors2.Panic(errors2.InsertOneError)
	}
	return res
}

func (s *Service) UpdateCustomer(customer _type.Customer) {
	res := s.repo.UpdateOne(customer)
	if res == 0 {
		errors2.Panic(errors2.CustomerNotFoundError)
	}
}

func (s *Service) DeleteCustomer(id primitive.ObjectID) {
	res := s.repo.DeleteOne(id)
	if res == 0 {
		errors2.Panic(errors2.CustomerNotFoundError)
	}
}
