package cmd

import (
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"github.com/swaggo/echo-swagger"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"orderapplication/src/customerhealthcheck"
	"orderapplication/src/docs"
	handler2 "orderapplication/src/handler"
	"orderapplication/src/pkg/config"
	"orderapplication/src/pkg/healthcheck"
	customMiddleware "orderapplication/src/pkg/helper/middleware"
	"orderapplication/src/service"
	"orderapplication/src/storage/mongoDb"
)

var (
	mclient     *mongo.Client
	mongoClient *mongo.Client
	db          *mongo.Database
	col         *mongo.Collection
	cfg         config.Config
)

func init() {
	cfg = config.EnvConfig["local"]
	connectionURI := fmt.Sprintf("mongodb+srv://%s:%s@cluster0.ng3fk.mongodb.net/%s?retryWrites=true&w=majority", cfg.CustomerUserName, cfg.CustomerPassword, cfg.DbName)
	clientOpts := options.Client().ApplyURI(connectionURI)
	mongoClient, conErr := mongo.Connect(context.Background(), clientOpts)
	if conErr != nil {
		log.Errorf("Somethings went wrong when connecting mongoDb.")
	}
	db = mongoClient.Database(cfg.DbName)
	col = db.Collection(cfg.CustomerCollectionName)
	mclient = mongoClient
}

func Execute() {
	e := echo.New()
	e.Use(customMiddleware.Recovery)
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))
	customerRepository := mongoDb.NewRepository(col)
	customerService := service.NewService(customerRepository)
	customerHandler := handler2.NewCustomerHandler(customerService)
	Route(e, customerHandler)
	healthCheck := healthcheck.NewHealthChecks()
	mongoCheck := customerhealthcheck.NewMongoHealthCheck(mclient)
	healthCheck.AddHealthChecks("mongoDB", &mongoCheck)
	healthHandler := handler2.NewHealthyHandler(healthCheck)
	e.GET("/healthy", healthHandler.CheckServicesHealth)
	e.GET("/quickhealthy", healthHandler.CheckQuickCustomerHealth)
	e.GET("/swagger/*", echoSwagger.WrapHandler)
	docs.SwaggerInfo.Title = "Customer Service"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Description = "Customer Service"
	docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", cfg.Host, cfg.Port)
	docs.SwaggerInfo.Schemes = []string{"http"}
	e.Logger.Infof("Listening on %s:%s", cfg.Host, cfg.Port)
	e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%s", cfg.Host, cfg.Port)))
}

func Route(e *echo.Echo, customerHandler *handler2.CustomerHandler) {
	e.GET("/customer", customerHandler.FindCustomersHandler)
	e.GET("/customer/:id", customerHandler.FindByIdHandler)
	e.GET("/customer/valid", customerHandler.IsCustomerIdValid)
	e.POST("/customer", customerHandler.CreateCustomerHandler)
	e.PUT("/customer/:id", customerHandler.UpdateCustomer)
	e.DELETE("/customer/:id", customerHandler.DeleteCustomer)
}
