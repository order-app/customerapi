package _type

import (
	"github.com/labstack/gommon/log"
	errors2 "orderapplication/src/pkg/helper/errors"
	"orderapplication/src/storage/type"
)

type RequestType struct {
	Name        string `json:"name" bson:"name"`
	Email       string `json:"email" bson:"email"`
	AddressLine string `json:"addressLine" bson:"addressLine"`
	City        string `json:"city" bson:"city"`
	Country     string `json:"country" bson:"country"`
	CityCode    int    `json:"cityCode" bson:"cityCode"`
}

func (c *RequestType) NewCustomerType() _type.Customer {
	var address _type.Address
	address.AddressLine = c.AddressLine
	address.Country = c.Country
	address.CityCode = c.CityCode
	address.City = c.City

	return _type.Customer{
		Name:    c.Name,
		Email:   c.Email,
		Address: address,
	}
}

func (c *RequestType) ValidateRequest() {
	if c.Name == "" {
		log.Errorf("Name is required, it can not be empty.")
		errors2.Panic(errors2.NameValidationError)
	}
	if c.Email == "" {
		log.Errorf("Email is required, it can not be empty.")
		errors2.Panic(errors2.EmailValidationError)
	}
	if c.AddressLine == "" {
		log.Errorf("AddressLine is required, it can not be empty.")
		errors2.Panic(errors2.AddressLineValidationError)
	}
	if c.Country == "" {
		log.Errorf("Country is required, it can not be empty.")
		errors2.Panic(errors2.CountryValidationError)
	}
	if c.City == "" {
		log.Errorf("City is required, it can not be empty.")
		errors2.Panic(errors2.CityValidationError)
	}
	if c.CityCode == 0 || c.CityCode < 0 {
		log.Errorf("CityCode is required, it can not be lower than 1.")
		errors2.Panic(errors2.CityCodeValidationError)
	}
}
