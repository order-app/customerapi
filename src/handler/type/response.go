package _type

import (
	mongoType "orderapplication/src/storage/type"
	"time"
)

type QuickResponseType struct {
	Result bool `json:"result"`
}

type IdResponseType struct {
	Id interface{} `json:"id"`
}

type CustomerResponseType struct {
	Name    string            `json:"name"`
	Email   string            `json:"email"`
	Address mongoType.Address `json:"address"`
}

type CustomersResponseType struct {
	TotalItemCount int                    `json:"totalItemCount"`
	PageNumber     int                    `json:"pageNumber"`
	PageSize       int                    `json:"pageSize"`
	Data           []CustomerResponseType `json:"data"`
}

type ErrorType struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type HealthCheckResponse struct {
	Status   string                    `json:"status"`
	Services []ServicesHealthyResponse `json:"services"`
}

type ServicesHealthyResponse struct {
	ServiceName   string    `json:"serviceName"`
	Status        string    `json:"serviceStatus"`
	LastCheckTime time.Time `json:"LastCheckTime"`
}

func CreateCustomerResponseType(customer *mongoType.Customer) *CustomerResponseType {
	return &CustomerResponseType{
		Name:  customer.Name,
		Email: customer.Email,
		Address: mongoType.Address{
			AddressLine: customer.Address.AddressLine,
			City:        customer.Address.City,
			Country:     customer.Address.Country,
			CityCode:    customer.Address.CityCode,
		},
	}
}

func CreateCustomersResponseType(total int, pageN int, perPage int, customers []mongoType.Customer) *CustomersResponseType {
	var customerList []CustomerResponseType
	for _, customer := range customers {
		customerResponseType := CreateCustomerResponseType(&customer)
		customerList = append(customerList, *customerResponseType)
	}
	return &CustomersResponseType{
		TotalItemCount: total,
		PageNumber:     pageN,
		PageSize:       perPage,
		Data:           customerList,
	}
}
