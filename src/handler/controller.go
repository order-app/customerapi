package handler

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"orderapplication/src/handler/type"
	errors2 "orderapplication/src/pkg/helper/errors"
	"orderapplication/src/service"
	helper2 "orderapplication/src/storage/helper"
	_mongoType "orderapplication/src/storage/type"
)

type CustomerHandler struct {
	service *service.Service
}

func NewCustomerHandler(s *service.Service) *CustomerHandler {
	return &CustomerHandler{service: s}
}

// FindByIdHandler
// @Summary      Show a customer
// @Description  get customer by ID
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Customer ID"
// @Success      200  {object}  _type.CustomerResponseType
// @Failure      400 {object}	_type.ErrorType
// @Failure      404 {object}	_type.ErrorType
// @Router       /customer/{id} [get]
func (h *CustomerHandler) FindByIdHandler(c echo.Context) error {
	id := c.Param("id")
	cID, errs := primitive.ObjectIDFromHex(id)
	if errs != nil {
		log.Errorf("Unable to convert to objectId")
		errors2.Panic(errors2.ConvertObjectIdError)
	}
	res := h.service.FindCustomerById(cID)
	return c.JSON(http.StatusOK, _type.CreateCustomerResponseType(res))
}

// FindCustomersHandler
//@Summary      List customers
// @Description  get customers
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        sort query string false "sort"
// @Param        page query int false "page"
// @Param		 name query string false "name"
// @Param		 email query string false "email"
// @Param		 address.addressLine query string false "address line"
// @Param		 address.city query string false "city"
// @Param 		 address.country query string false "country"
// @Param		 address.cityCode query int false "city code"
// @Success      200  {object}  _type.CustomersResponseType
// @Failure		 404 {object} 	_type.ErrorType
// @Router       /customer 	[get]
func (h *CustomerHandler) FindCustomersHandler(c echo.Context) error {
	pageN, perPage, options := helper2.SetOptions(c.QueryParams())
	filter := helper2.MakeFilter(c.QueryParams())
	total, res := h.service.FindCustomers(options, filter)
	return c.JSON(http.StatusOK, _type.CreateCustomersResponseType(total, pageN, perPage, res))
}

func (h *CustomerHandler) IsCustomerIdValid(c echo.Context) error {
	id := c.QueryParam("id")
	cID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Errorf("Unable to convert to objectId : %s", err)
		errors2.Panic(errors2.ConvertObjectIdError)
	}
	h.service.IsCustomerIdValid(cID)
	return c.JSON(http.StatusOK, _type.QuickResponseType{Result: true})
}

// CreateCustomerHandler
//@Summary       Add a customer
// @Description  create customer
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param 	_type.RequestType body _type.RequestType true "For create to Customer"
//@Success      201  {object}  _type.IdResponseType
// @Failure		 400 {object} 	_type.ErrorType
// @Failure		 500 {object} 	_type.ErrorType
// @Router       /customer [post]
func (h *CustomerHandler) CreateCustomerHandler(c echo.Context) error {
	var customer _mongoType.Customer
	var createReq _type.RequestType
	if bindErr := c.Bind(&createReq); bindErr != nil {
		log.Errorf("Unable to bind : %v", bindErr)
		errors2.Panic(errors2.CustomerBindError)
	}
	createReq.ValidateRequest()
	customer = createReq.NewCustomerType()
	cID := h.service.CreateCustomer(customer)
	return c.JSON(http.StatusCreated, _type.IdResponseType{Id: cID})
}

// UpdateCustomer
//@Summary       Update a customer
// @Description  update customer
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Customer ID"
// @Param 		_type.RequestType body _type.RequestType true "For update to Customer"
// @Success      200  {object}  _type.QuickResponseType
// @Failure		 404 {object} 	_type.ErrorType
// @Failure		 400 {object} 	_type.ErrorType
// @Router       /customer/{id} [put]
func (h *CustomerHandler) UpdateCustomer(c echo.Context) error {
	var customer _mongoType.Customer
	var updateReq _type.RequestType
	id := c.Param("id")
	reqId, idErr := primitive.ObjectIDFromHex(id)
	if idErr != nil {
		log.Errorf("Unable to convert the id : %v", id)
		errors2.Panic(errors2.ConvertObjectIdError)
	}
	if err := c.Bind(&updateReq); err != nil {
		log.Errorf("Unable to bind : %v", err)
		errors2.Panic(errors2.CustomerBindError)
	}
	updateReq.ValidateRequest()
	customer = updateReq.NewCustomerType()
	customer.Id = reqId
	h.service.UpdateCustomer(customer)
	return c.JSON(http.StatusOK, _type.QuickResponseType{Result: true})
}

// DeleteCustomer
//@Summary       Delete a customer
// @Description  delete customer by ID
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Customer ID"
// @Success      200 {object}  _type.QuickResponseType
//@Failure      400  {object}  _type.ErrorType
// @Failure		 404 {object} 	_type.ErrorType
// @Router       /customer/{id} [delete]
func (h *CustomerHandler) DeleteCustomer(c echo.Context) error {
	id := c.Param("id")
	reqId, reqErr := primitive.ObjectIDFromHex(id)
	if reqErr != nil {
		log.Errorf("Unable to convert the objectId : %v", reqErr)
		errors2.Panic(errors2.ConvertObjectIdError)
	}
	h.service.DeleteCustomer(reqId)
	return c.JSON(http.StatusOK, _type.QuickResponseType{Result: true})
}
