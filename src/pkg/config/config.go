package config

type Config struct {
	Host                   string
	Port                   string
	CustomerUserName       string
	CustomerPassword       string
	CustomerCollectionName string
	DbName                 string
}

var EnvConfig = map[string]Config{
	"local": {
		Host:                   "localhost",
		Port:                   "8080",
		CustomerUserName:       "ordercustomer",
		CustomerPassword:       "12345",
		CustomerCollectionName: "customers",
		DbName:                 "orderapplicationDB"},
	"qa": {
		Host:                   "deneme",
		Port:                   "deneme",
		CustomerUserName:       "deneme",
		CustomerPassword:       "deneme",
		CustomerCollectionName: "deneme",
		DbName:                 "deneme",
	},
}
