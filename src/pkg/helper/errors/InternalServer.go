package errors

import (
	"net/http"
	"orderapplication/src/pkg/helper/errors/type"
)

var (
	InsertOneError = _type.ErrorType{
		Code:    http.StatusInternalServerError,
		Message: "InsertOne error.",
	}

	UpdateOneError = _type.ErrorType{
		Code:    http.StatusInternalServerError,
		Message: "UpdateOne error.",
	}

	DeleteOneError = _type.ErrorType{
		Code:    http.StatusInternalServerError,
		Message: "DeleteOne error.",
	}
)
