package errors

import (
	"orderapplication/src/pkg/helper/errors/type"
)

func Panic(err _type.ErrorType) {
	panic(&_type.ErrorType{
		Code:    err.Code,
		Message: err.Message,
	})
}
