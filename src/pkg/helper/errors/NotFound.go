package errors

import (
	"net/http"
	"orderapplication/src/pkg/helper/errors/type"
)

var (
	CustomerNotFoundError = _type.ErrorType{
		Code:    http.StatusNotFound,
		Message: "Customer not found in database",
	}
)
