package errors

import (
	"net/http"
	"orderapplication/src/pkg/helper/errors/type"
)

var (
	CustomerBindError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Unable to bind the request body.",
	}

	ConvertObjectIdError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Cannot convert to ObjectId",
	}

	CityCodeConvertError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Convert error",
	}

	NameValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Name is required, it can not be empty.",
	}

	EmailValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Email is required, it can not be empty.",
	}

	AddressLineValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "AddressLine is required, it can not be empty.",
	}

	CountryValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Country is required, it can not be empty.",
	}

	CityValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "City is required, it can not be empty.",
	}

	CityCodeValidationError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "CityCode is required, it can not be lower than 1.",
	}
)
