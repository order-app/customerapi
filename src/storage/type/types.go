package _type

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Customer struct {
	Id        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name      string             `json:"name" bson:"name"`
	Email     string             `json:"email" bson:"email"`
	Address   Address            `json:"address" bson:"address"`
	CreatedAt time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time          `json:"updatedAt" bson:"updatedAt"`
}

type Address struct {
	AddressLine string `json:"addressLine" bson:"addressLine"`
	City        string `json:"city" bson:"city"`
	Country     string `json:"country" bson:"country"`
	CityCode    int    `json:"cityCode" bson:"cityCode"`
}
