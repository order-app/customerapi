package helper

import (
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/url"
	errors2 "orderapplication/src/pkg/helper/errors"
	"strconv"
)

func MakeFilter(values url.Values) map[string]interface{} {
	var filter = make(map[string]interface{})
	for i, v := range values {
		filter[i] = v[0]
		switch {
		case i == "_id":
			id, err := primitive.ObjectIDFromHex(filter["_id"].(string))
			if err != nil {
				log.Errorf("Cannot convert to ObjectId")
				errors2.Panic(errors2.ConvertObjectIdError)
			}
			filter["_id"] = id
		case i == "address.cityCode":
			cityCode, err := strconv.Atoi(filter["address.cityCode"].(string))
			if err != nil {
				log.Errorf("Convert error : %s", err)
				errors2.Panic(errors2.CityCodeConvertError)
			}
			filter[i] = cityCode
		}
	}
	return filter
}
