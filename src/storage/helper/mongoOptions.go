package helper

import (
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
	"net/url"
	"strconv"
)

func SetOptions(values url.Values) (int, int, *options.FindOptions) {
	findOptions := options.Find()
	if sort := values.Get("sort"); sort != "" {
		switch sort {
		case "asc":
			findOptions.SetSort(bson.M{"name": 1})
		case "desc":
			findOptions.SetSort(bson.M{"name": -1})
		}
	}
	values.Del("sort")
	page, _ := strconv.Atoi(values.Get("page"))
	if page == 0 {
		page = 1
	}
	var perPage int64 = 4
	findOptions.SetSkip((int64(page) - 1) * perPage)
	findOptions.SetLimit(perPage)
	values.Del("page")
	return page, int(perPage), findOptions
}
