package mongoDb

import (
	"context"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
	"orderapplication/src/storage/mongoDb/dbiface"
	"orderapplication/src/storage/type"
	"time"
)

type Repository struct {
	collection dbiface.MongoCollection
}

func NewRepository(collection dbiface.MongoCollection) *Repository {
	return &Repository{collection: collection}
}

func (r *Repository) FindById(id primitive.ObjectID) *_type.Customer {
	var customer _type.Customer
	filter := bson.M{"_id": id}
	res := r.collection.FindOne(context.Background(), filter)
	if err := res.Decode(&customer); err != nil {
		log.Errorf("Object with id : %s not found in database.", id.String())
		return nil
	}
	return &customer
}
func (r *Repository) FindMany(filter map[string]interface{}, options *options.FindOptions) (int64, []_type.Customer) {
	var customers []_type.Customer
	res, colErr := r.collection.Find(context.Background(), bson.M(filter), options)
	if colErr != nil {
		log.Errorf("Find error : %s", colErr)
		return 0, nil
	}
	if cursError := res.All(context.Background(), &customers); cursError != nil {
		log.Errorf("Unable to read the cursor : %s", cursError)
		return 0, nil
	}
	total, _ := r.collection.CountDocuments(context.Background(), filter)
	return total, customers
}

func (r *Repository) IsIdValid(id primitive.ObjectID) int64 {
	res, err := r.collection.CountDocuments(context.Background(), bson.M{"_id": id})
	if err != nil {
		log.Errorf("Unable to count to object : %s", err)
		return 0
	}
	return res
}

func (r *Repository) InsertOne(customer _type.Customer) interface{} {
	customer.CreatedAt = time.Now()
	customer.UpdatedAt = time.Now()
	res, err := r.collection.InsertOne(context.Background(), customer)
	if err != nil {
		log.Errorf("Unable to insert the object")
		return nil
	}
	return res.InsertedID
}

func (r *Repository) UpdateOne(req _type.Customer) int64 {
	filter := bson.M{"_id": req.Id}
	req.UpdatedAt = time.Now()
	update := bson.M{"$set": bson.M{"name": req.Name, "email": req.Email,
		"address.addressLine": req.Address.AddressLine,
		"address.city":        req.Address.City, "address.country": req.Address.Country,
		"address.cityCode": req.Address.CityCode, "updatedAt": req.UpdatedAt}}
	res, err := r.collection.UpdateOne(context.Background(), filter, &update)
	if err != nil {
		log.Errorf("UpdateOne error : %s", err)
		return 0
	}
	return res.ModifiedCount
}

func (r *Repository) DeleteOne(id primitive.ObjectID) int64 {
	filter := bson.M{"_id": id}
	res, err := r.collection.DeleteOne(context.Background(), filter)
	if err != nil {
		log.Errorf("DeleteOne error : %s", err)
		return 0
	}
	return res.DeletedCount
}
